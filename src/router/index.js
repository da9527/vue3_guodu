import { createRouter, createWebHistory } from "vue-router";

const routes = [
	// 没找到的路由
	{
		path: "/:pathMatch(.*)*",
		component: () => import("../components/NotFound.vue"),
	},
	{
		path: "/",
		component: () => import("../components/Hello.vue"),
	},
	{
		path: "/hi",
		component: () => import("../components/Hi.vue"),
	},
];

export default createRouter({
	history: createWebHistory(),
	routes,
});
